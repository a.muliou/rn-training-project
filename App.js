/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import CongratulationScreen from './src/screens/CongratulationScreen';

const App: () => React$Node = () => {
  return (
    <View style={styles.mainScreen}>
      <StatusBar />
      <CongratulationScreen />
    </View>
  );
};

const styles = StyleSheet.create({
  mainScreen: {
    flex: 1,
    flexDirection: 'column',
  },
});

export default App;
