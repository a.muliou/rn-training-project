import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  Dimensions,
  FlatList,
} from 'react-native';
import ButtonGradient from '../components/ButtonGradient';
import NavBar from '../components/NavBar';

const CongratulationScreen = () => {
  return (
    <View style={styles.mainScreen}>
      <View style={styles.wrapper}>
        <Image
          style={styles.pictureCatColor}
          source={require('../../assets/picture-cat-color.png')}
        />
        <Text style={styles.header}>Congratulations!</Text>
        <Text style={styles.message}>Now you can write your first story!</Text>
        <ButtonGradient />
      </View>
      <NavBar />
    </View>
  );
};

export default CongratulationScreen;

const styles = StyleSheet.create({
  mainScreen: {
    backgroundColor: '#2a2b30',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: Platform.OS === 'ios' ? 31 : 0,
    // height: Dimensions.get('window').height,
    // height: '100%',
  },
  wrapper: {
    alignItems: 'center',
  },
  pictureCatColor: {
    width: 351,
    height: 102,
    resizeMode: 'contain',
  },
  header: {
    marginTop: 100,
    width: 281,
    // fontFamily: 'OpenSans',
    fontSize: 22,
    fontWeight: 'normal',
    textAlign: 'center',
    color: '#ffffff',
    letterSpacing: -0.71,
  },
  message: {
    marginTop: 10,
    width: 281,
    fontSize: 32,
    fontWeight: '600',
    textAlign: 'center',
    color: '#ffffff',
    letterSpacing: -1.03,
  },
});
