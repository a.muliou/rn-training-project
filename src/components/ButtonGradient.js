import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const ButtonGradient = () => {
  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={['#00ffc3', '#554ede']}
      style={styles.button}>
      <Text style={styles.buttonMessage}>Let’s start! ✍️</Text>
    </LinearGradient>
  );
};

export default ButtonGradient;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 325,
    height: 50,
    borderRadius: 25,
    marginTop: 165,
  },
  buttonMessage: {
    width: 129,
    height: 29,
    textShadowColor: 'rgba(0, 0, 0, 0.25)',
    textShadowOffset: {width: 0, height: 2},
    textShadowRadius: 4,
    fontSize: 20,
    fontWeight: '600',
    color: '#ffffff',
    textAlign: 'center',
  },
});
