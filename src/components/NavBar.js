import React from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  Image,
  Text,
  Platform,
} from 'react-native';

const NavBar = () => {
  return (
    <View style={styles.navBar}>
      <View style={styles.item}>
        <Image
          source={require('../../assets/icons/icon-trends.png')}
          style={styles.itemImage}
        />
        <Text style={styles.itemMessage}>Trends</Text>
      </View>
      <View style={styles.item}>
        <Image
          source={require('../../assets/icons/icon-reading.png')}
          style={styles.itemImage}
        />
        <Text style={styles.itemMessage}>Reading</Text>
      </View>
      <View style={styles.item}>
        <Image
          source={require('../../assets/icons/icon-categories.png')}
          style={styles.itemImage}
        />
        <Text style={styles.itemMessage}>Categories</Text>
      </View>
      <View style={styles.item}>
        <Image
          source={require('../../assets/icons/icon-profile-active.png')}
          style={styles.itemImage}
        />
        <Text style={styles.itemMessage}>Profile</Text>
      </View>
    </View>
  );
};

export default NavBar;

const styles = StyleSheet.create({
  navBar: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Platform.OS === 'android' ? 49 : 80,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  item: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 49,
    width: 47,
    paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  itemImage: {
    width: '100%',
    height: 31,
    resizeMode: 'contain',
  },
  itemMessage: {
    fontSize: 10,
    fontWeight: 'normal',
    letterSpacing: -0.32,
    color: '#b5b5ba',
    textAlign: 'center',
  },
});
